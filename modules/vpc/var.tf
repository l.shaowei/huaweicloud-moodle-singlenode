## Region and Availability zone variables ##

variable "region" {
    default = ""
}


## Network variables ##

variable "vpc_name" {
    default = ""
}

variable "vpc_cidr" {
    default = ""
}

variable "subnet_name" {
    default = ""
}

variable "subnet_cidr" {
    default = ""
}

variable "subnet_gateway_ip" {
    default = ""
}

variable "dns_list" {
    default = ""
}


variable "remote_ip_prefix" {
    default = ""
}

## application environment variables ##
variable "tags" {
    default = {
    }
}

## Instance variables ##

variable "app_name" {
    default = ""
}

variable "environment" {
  default = ""
}


## Security Group variable ##

variable "ports-ranges" {
  description = "Port ranges to create on security group rule"
  default = [""]
}

